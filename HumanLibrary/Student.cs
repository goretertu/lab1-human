﻿namespace v3_human_library;

public class Student : Human
{
    public string CordbookNumber { get; protected set; }
    public string Group { get; protected set; }

    public Student(string name, int age, string sex, float height, float weight, string creed, string nationality,
        string profession, string numberOfPassport, string cordbookNumber, string group)
        : base(name, age, sex, height, weight, creed, nationality, profession, numberOfPassport)
    {
        CordbookNumber = cordbookNumber;
        Group = group;
    }

    public override string ToString()
    {
        return $"Student:\n{Name}\n{Age}\n{Sex}\n{Height}\n{Weight}" +
               $"\n{Creed}\n{Nationality}\n{NumberOfPassport}\n{CordbookNumber}\n{Group}\n";
    }
}