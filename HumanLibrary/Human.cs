﻿namespace v3_human_library;

public abstract class Human
{
    public string Name { get; protected set; }
    public int Age { get; protected set; }
    public string Sex { get; protected set; }
    public float Height { get; protected set; }
    public float Weight { get; protected set; }
    public string Creed { get; protected set; }
    public string Nationality { get; protected set; }
    public string Profession { get; protected set; }
    public string NumberOfPassport { get; protected set; }
    public double BodyMassIndex { get; protected set; }

    public Human(string name, int age, string sex, float height, float weight, string creed, string nationality,
        string profession, string numberOfPassport)
    {
        (Name, Age, Sex, Height, Weight) = (name, age, sex, height, weight);
        (Creed, Nationality, Name, Profession, NumberOfPassport) =
            (creed, nationality, name, profession, numberOfPassport);

        BodyMassIndex = weight / Math.Pow(height, 2);
    }
}