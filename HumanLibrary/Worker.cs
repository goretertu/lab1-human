﻿namespace v3_human_library;

public class Worker : Human
{
    public string Department { get; protected set; }
    public string Position { get; protected set; }

    public Worker(string name, int age, string sex, float height, float weight, string creed, string nationality,
        string profession, string numberOfPassport, string position, string department) :
        base(name, age, sex, height, weight, creed, nationality, profession, numberOfPassport)
    {
        Department = department;
        Position = position;
    }

    public override string ToString()
    {
        return $"Worker:\n{Name}\n{Age}\n{Sex}\n{Height}\n{Weight}\n" +
               $"{Creed}\n{Nationality}\n{Profession}\n{NumberOfPassport}\n{Position}\n{Department}\n";
    }
}