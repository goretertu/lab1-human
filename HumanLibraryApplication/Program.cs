﻿using v3_human_library;

var student = new Student(
    "student",
    20,
    "Men",
    175,
    70,
    "christian",
    "ukrainian",
    "developer",
    "0101",
    "30",
    "CS30");

var worker = new Worker(
    "worker",
    35,
    "Woman",
    165,
    70,
    "christian",
    "ukrainian",
    "teacher",
    "010101",
    "teacher",
    "CS30");

Console.WriteLine(student);
Console.WriteLine(worker);

Console.Write("Press any button");
Console.ReadKey();